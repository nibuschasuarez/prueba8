import { Injectable } from '@angular/core';
import { ICard } from '../interfaces/card.interfaces';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  listCards: ICard[] = []

  constructor() { }

  getCards() {
    return this.listCards;
  }

  addCard(card: ICard) {
    this.listCards.unshift(card)
  }

  editCard(card: ICard, id: number) {
    this.listCards[id] = card
  }

  deleteCard(i: number) {
    this.listCards.splice(i, 1)
  }
}
