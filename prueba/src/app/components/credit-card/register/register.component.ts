import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ICard } from 'src/app/interfaces/card.interfaces';
import { CardsService } from 'src/app/services/cards.service';
import { Card } from 'src/app/models/card';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  @Input() subject?: Subject<{ edit: boolean, id: number, data: Card }>;
  title= "Agregar Tarjeta"
  infos: { edit: boolean, id: number, data:{
    titular: string,
    numCard: number,
    date: string,
    currentPassword: string} } = { edit: false, id: 0,
    data: new Card
  };

  form!: FormGroup;
  hide = true;

  edit=false

  constructor(fb: FormBuilder, private serviceCards: CardsService) {
    this.form = fb.group({
      titular: ["", [Validators.required, Validators.pattern('[A-Za-z]+$')]],
      numCard: ["", [Validators.required, Validators.pattern('[0-9]{12,12}')]],
      date: ["", [Validators.required, Validators.pattern('[0-9]{2}/[0-9]{2}')]],
      currentPassword: ["", [Validators.required]]
    })
  }

  handleSubmit(){
    if (this.form.valid) {
      if (this.infos.edit) {
        this.editCard()
      } else {
        this.addAcount()
      }
    }
  }

  addAcount() {
    let newCard: ICard = {
      titular: this.form.value.titular.toUpperCase(),
      numCard: this.form.value.numCard,
      date: this.form.value.date,
      currentPassword: this.form.value.currentPassword
    }
    this.serviceCards.addCard(newCard)
    this.form.reset()
  }

  editCard() {
    let newCard: ICard = {
      titular: this.form.value.titular.toUpperCase(),
      numCard: this.form.value.numCard,
      date: this.form.value.date,
      currentPassword: this.form.value.currentPassword
    }
    this.serviceCards.editCard(newCard, this.infos.id)
    this.title= "Agregar Tarjeta"
    this.infos.edit = false
    this.form.reset()
  }

  ngOnInit(): void {
    this.subject?.subscribe((text: { edit: boolean, id: number, data: Card }) => {
      this.title = "Editar Tarjeta"
      this.infos.id= text.id
      let datos = text.data
      this.form.setValue({
        titular: datos.titular,
        numCard: datos.numCard,
        date: datos.date,
        currentPassword: datos.currentPassword
      });
    });
  }

}
