export class Card {
  titular: string
  numCard: number
  date: string
  currentPassword: string

  constructor() {
    this.titular = ""
    this.numCard = 0
    this.date = ""
    this.currentPassword = ""
  }
}