import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Card } from 'src/app/models/card';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.css']
})
export class CreditCardComponent implements OnInit {
  subject = new Subject<{ edit: boolean, id: number, data: Card }>();

  constructor() { }

  ngOnInit(): void {
    this.subject.subscribe((text: { edit: boolean, id: number, data: Card }) => {
      console.log(`Received from child component: ${text}`);
    });
  }
  
  handleInfo = (info: { edit: boolean, id: number, data: Card }) => {
    this.subject.next(info);
  };

}
