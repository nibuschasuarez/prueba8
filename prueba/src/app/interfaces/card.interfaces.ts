export interface ICard {
  titular:string,
  numCard: number,
  date: string,
  currentPassword: string
}
