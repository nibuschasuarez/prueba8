import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ICard } from 'src/app/interfaces/card.interfaces';
import { CardsService } from 'src/app/services/cards.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogoConfirmacionComponent } from 'src/app/dialogo-confirmacion/dialogo-confirmacion.component';
import { Card } from 'src/app/models/card';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Output() onInfo: EventEmitter<{ edit: boolean, id: number, data: Card }> = new EventEmitter<{
    edit: boolean, id: number, data: Card
  }>();
  text?: { edit: boolean,id: number, data: Card };

  list: ICard[] = []

  constructor(private serviceCards: CardsService, public dialogo: MatDialog) {
    this.list = serviceCards.getCards()
  }

  
  sendInfo = (): void => {
    this.onInfo.emit(this.text);
    this.text = { 
      edit: false, 
      id: 0,
      data: {
        titular : "",
        numCard : 0,
        date : "",
        currentPassword : ""
      }
    };
  };

  editCard(i: number) {
    let element = this.list[i]
    this.text = {
      edit: true, 
      id: i,
      data: {
        titular : element.titular,
        numCard : element.numCard,
        date : element.date,
        currentPassword : element.currentPassword
      }
    }

    this.onInfo.emit(this.text);
  }

  deleted(i: number) {
    this.serviceCards.deleteCard(i)
  }
  
  mostrarDialogo(): void {
    this.dialogo
      .open(DialogoConfirmacionComponent, {
        data: `¿Te gusta programar en TypeScript? https://parzibyte.me/blog/2019/11/08/dialogo-confirmacion-angular-material/`
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (confirmado) {
          alert("¡A mí también!");
        } else {
          alert("Deberías probarlo, a mí me gusta :)");
        }
      });
  }

  ngOnInit(): void {
  }

}
